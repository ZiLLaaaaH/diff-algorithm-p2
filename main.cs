DiffEngine de = new DiffEngine();
de.WindowSize = 10;        // very small window for a small script

// create string readers
ComparableStringReader csrSource = new ComparableStringReader(txtSource.Text);
ComparableStringReader csrDest = new ComparableStringReader(txtDestination.Text);

// do the compare
EditScript es = de.Compare(csrSource, csrDest);
